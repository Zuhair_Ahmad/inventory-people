#Install

1) composer update
2) npm install

#Migration
1) php artisan migrate

#Seeder

1) php artisan db:seed --class=AdminSeeder
