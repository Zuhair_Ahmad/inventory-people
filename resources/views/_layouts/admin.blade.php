<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8"/>
    <title>Inventory People</title>
    <meta name="description" content="Updates and statistics">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->
@include('_partials.css-assets')
<!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{asset('assets/media/logos/icon.png')}}"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
        <div
            class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

            <!--begin::Aside-->
            <div
                class="leftFixed kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside"
                style="background-image: url({{asset('assets/media/bg/bg-9.jpg')}}) ; ">
                <div class="kt-grid__item">
                    <a href="#" class="kt-login__logo">
                        <img src="{{asset('assets/media/logos/inventoryLogo.png')}}" style="width: 250px">
                    </a>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                    <div class="kt-grid__item kt-grid__item--middle">
                        <h3 class="kt-login__title">Welcome to Inventory People!</h3>
                        <h4 class="kt-login__subtitle">We provide retailers with experienced inventory people, whatever
                            the need!</h4>
                    </div>
                </div>
                <div class="kt-grid__item">
                    <div class="kt-login__info">
                        <div class="kt-login__copyright">
                            &copy 2020 Inventory People
                        </div>
                    </div>
                </div>
            </div>

            <!--begin::Aside-->

            <!--begin::Content-->
            <div class="rightFixed kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

                <!--begin::Head-->
                <div class="kt-login__head">
                    <span class="kt-login__signup-label">Don't have an account yet?</span>&nbsp;&nbsp;
                    @if (Route::has('register'))
                        <span class="accountLink"><a class="kt-link kt-login__signup-link"
                                                     href="{{ route('register') }}">{{ __('Register') }}</a></span>
                    @endif
                    <span class="accountLink"><a class="kt-link kt-login__signup-link"
                                                 href="{{ route('login') }}">{{ __('Login') }}</a></span>

                </div>

                <!--end::Head-->

                <!--begin::Body-->
                <div>
                    <!--begin::Form-->
                @yield('content')
                <!--end::Form-->
                </div>
                <!--end::Options-->

                <!--end::Body-->
            </div>

            <!--end::Content-->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/plugins.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/scripts.bundle.js') }}" type="text/javascript"></script>
{{--<script src="{{ asset('js/wizard-1.js') }}" type="text/javascript"></script>--}}
<script src="{{ asset('js/profile.js') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
</body>
<style>
    .kt-login.kt-login--v1 .kt-login__aside .kt-login__title {
        color: #335377d4 !important;
    }

    .kt-login.kt-login--v1 .kt-login__aside .kt-login__subtitle {
        color: #335377d4 !important;
    }

    .kt-login.kt-login--v1 .kt-login__aside .kt-login__info .kt-login__copyright {
        color: #67666e !important;
    }

    .leftFixed {
        position: fixed;
        width: 45% !important;
        height: 100vh;
        left: 0;
        background-size: cover;
    }
    .rightFixed { position: fixed;
        right: 0;
        overflow-y: scroll;
        height: 100%;
        width:65%;}
</style>
<!-- end::Body -->
</html>
