@extends('_layouts.admin')
@section('form-title', 'Register')
@section('content')
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Register </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            @include('_layouts.flash-message')
            @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-toolbar">
                    <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
                        role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_user_edit_tab_1" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                     viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                        <path
                                            d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z"
                                            fill="#000000" fill-rule="nonzero"/>
                                        <path
                                            d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z"
                                            fill="#000000" opacity="0.3"/>
                                    </g>
                                </svg>
                                Profile
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_user_edit_tab_2" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                     viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                        <path
                                            d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <path
                                            d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                            fill="#000000" fill-rule="nonzero"/>
                                    </g>
                                </svg>
                                Address
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_user_edit_tab_3" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                     viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path
                                            d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z"
                                            fill="#000000" opacity="0.3"/>
                                        <path
                                            d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z"
                                            fill="#000000" opacity="0.3"/>
                                        <path
                                            d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z"
                                            fill="#000000" opacity="0.3"/>
                                    </g>
                                </svg>
                                Experience
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_user_edit_tab_4" role="tab">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                     viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path
                                            d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z"
                                            fill="#000000" opacity="0.3"/>
                                        <path
                                            d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z"
                                            fill="#000000"/>
                                    </g>
                                </svg>
                                Availability
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form class="kt-form" id="kt_form" method="POST" enctype="multipart/form-data"
                      action="{{ route('register') }}">
                    @csrf
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class=" kt-section__content--solid">
                                                        <h3 class="kt-section__title kt-section__title-sm active btn btn-label-info-o2">
                                                            Information :
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Image</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="form-group row">
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="kt-avatar kt-avatar--outline"
                                                                 id="kt_user_avatar">
                                                                <div class="kt-avatar__holder"
                                                                     style="background-image: url({{asset('assets/media/users/100_13.jpg')}})"></div>
                                                                <label class="kt-avatar__upload"
                                                                       data-toggle="kt-tooltip"
                                                                       title="" data-original-title="Change avatar">
                                                                    <i class="fa fa-pen"></i>
                                                                    <input type="file" name="image">
                                                                </label>
                                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                                      title="" data-original-title="Cancel avatar">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                                @error('image')
                                                                <span class="invalid-feedback" role="alert">
                                                                     <strong>{{ $message }}</strong>
                                                                 </span>
                                                                @enderror
                                                                <p style="color: #ff0000">Profile Image is required
                                                                    !</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">First
                                                    Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="firstName" id="firstName"
                                                           class="form-control @error('firstName') is-invalid @enderror"
                                                           value="{{ old('firstName') }}"
                                                           required autocomplete="firstName" autofocus>
                                                    @error('firstName')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Last
                                                    Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" name="lastName" id="lastName"
                                                           class="form-control @error('lastName') is-invalid @enderror"
                                                           value="{{ old('lastName') }}" required
                                                           autocomplete="lastName" autofocus>
                                                    @error('lastName')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input id="email" type="email"
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           name="email"
                                                           value="{{ old('email') }}" required
                                                           autocomplete="email">
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input id="password" type="password"
                                                           class="form-control @error('password') is-invalid @enderror"
                                                           name="password" autocomplete="new-password">
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                                 <strong>{{ $message }}</strong>
                                                             </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Confirm
                                                    Password</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input id="password-confirm" type="password"
                                                           class="form-control" name="password_confirmation"
                                                           autocomplete="new-password">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Contact
                                                    Phone</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="number" id="phone"
                                                           class="form-control @error('phone') is-invalid @enderror"
                                                           name="phone"
                                                           value="{{ old('phone') }}" autocomplete="phone"
                                                           required
                                                           autofocus>
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Mobile</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <input type="number" id="mobile"
                                                               class="form-control @error('mobile') is-invalid @enderror"
                                                               name="mobile"
                                                               value="{{ old('mobile') }}" required
                                                               autocomplete="mobile" autofocus>
                                                        @error('mobile')
                                                        <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6">
                                    <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
                                        role="tablist">
                                        <li class="nav-item">
                                            <a class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                               data-toggle="tab" href="#kt_user_edit_tab_1" role="tab">
                                                Previous
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                               data-toggle="tab" href="#kt_user_edit_tab_2" role="tab">
                                                Next
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_user_edit_tab_2" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class=" kt-section__content--solid">
                                                        <h3 class="kt-section__title kt-section__title-sm active btn btn-label-info-o2">
                                                            Address :
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Street</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <input type="text" name="street" id="street"
                                                               class="form-control @error('street') is-invalid @enderror"
                                                               required value="{{ old('street') }}"
                                                               autocomplete="street" autofocus>
                                                        @error('street')
                                                        <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                                 </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Door
                                                    Number</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <input type="text" id="doorNumber"
                                                               class="form-control @error('doorNumber') is-invalid @enderror"
                                                               name="doorNumber"
                                                               value="{{ old('doorNumber') }}" required
                                                               autocomplete="doorNumber" autofocus>
                                                        @error('doorNumber')
                                                        <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                                    </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label
                                                    class="col-xl-3 col-lg-3 col-form-label">Appartement</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" id="appartement"
                                                           class="form-control @error('appartement') is-invalid @enderror"
                                                           name="appartement"
                                                           value="{{ old('appartement') }}" required
                                                           autocomplete="appartement" autofocus>
                                                    @error('appartement')
                                                    <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" id="city"
                                                           class="form-control @error('city') is-invalid @enderror"
                                                           name="city"
                                                           value="{{ old('city') }}"
                                                           required autocomplete="city" autofocus>
                                                    @error('city')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Postal Code/Zip
                                                    Code</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" id="postalCode"
                                                           class="form-control @error('postalCode') is-invalid @enderror"
                                                           name="postalCode"
                                                           value="{{ old('postalCode') }}"
                                                           required autocomplete="postalCode" autofocus>
                                                    @error('postalCode')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group  row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Country</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select id="country"
                                                            class="form-control @error('country') is-invalid @enderror"
                                                            name="country" value="" required autocomplete="country"
                                                            autofocus>
                                                        <option value="Canada">Canada</option>
                                                        <option value="US">US</option>
                                                    </select>
                                                    @error('country')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group  row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea id="address"
                                                              class="form-control @error('address') is-invalid @enderror"
                                                              name="address"  required
                                                              autocomplete="address"
                                                              autofocus>{{ old('address') }}</textarea>
                                                    @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6">
                                    <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
                                        role="tablist">
                                        <li class="nav-item">
                                            <a class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                               data-toggle="tab" href="#kt_user_edit_tab_1" role="tab">
                                                Previous
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                               data-toggle="tab" href="#kt_user_edit_tab_3" role="tab">
                                                Next
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_user_edit_tab_3" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm active btn btn-label-info-o2">
                                                        Experience :
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="form-group row mt-4">
                                                <label class="col-xl-3 col-lg-3 col-form-label">
                                                    Inventory experience
                                                </label>
                                                <div class="form-group mt-2">
                                                    <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                        <input type="radio" name="inventoryExperience" required
                                                               onchange="getExperienceValue(this.value)"
                                                               value="Yes">Yes
                                                        <span></span>
                                                    </label> &nbsp;&nbsp;&nbsp;
                                                    <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                        <input type="radio" name="inventoryExperience" required
                                                               onchange="getExperienceValue(this.value)"
                                                               value="No">No
                                                        <span></span>
                                                    </label>
                                                    @error('inventoryExperience')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                    <span class="form-text text-muted">Please select one.</span>
                                                </div>
                                            </div>
                                            <div class="form-group row ">
                                                <label class="col-xl-3 col-lg-3 col-form-label trialEndsAt"
                                                       style="display: none;">Company</label>
                                                <div class="col-lg-9 col-xl-6 trialEndsAt" style="display: none;">
                                                    <select class="form-control" name="experienceCompanyName"
                                                            id="experienceCompanyName" onclick="changeType()">
                                                        <option value="RGIS">RGIS</option>
                                                        <option value="WISI">WISI</option>
                                                        <option value="PICS">PICS</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                    @error('experienceCompanyName')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                            </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group  row ">
                                                <label class="col-xl-3 col-lg-3 col-form-label mt-3  typeField"
                                                       style="display: none">Specify Company</label>
                                                <div class="col-lg-9 col-xl-6  typeField" style="display: none">
                                                    <input type="text" name="otherCompany" class="form-control"
                                                           value="{{ old('otherCompany') }}">
                                                    @error('otherCompany')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row ">
                                                <label class="col-xl-3 col-lg-3 col-form-label trialEndsAt"
                                                       style="display: none;">Level</label>
                                                <div class="col-lg-9 col-xl-6 trialEndsAt" style="display: none;">
                                                    <select class="form-control" name="level">
                                                        <option value="Manager">Manager</option>
                                                        <option value="Team Leader">Team Leader</option>
                                                        <option value="Auditor">Auditor</option>
                                                    </select>
                                                    @error('level')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label trialEndsAt"
                                                       style="display: none;">Years Of
                                                    Experience </label>
                                                <div class="col-lg-9 col-xl-6 trialEndsAt" style="display: none;">
                                                    <input type="number" id="yearsOfExperience"
                                                           class="form-control @error('yearsOfExperience') is-invalid @enderror"
                                                           name="yearsOfExperience"
                                                           value="{{ old('yearsOfExperience') }}"
                                                           autocomplete="yearsOfExperience" autofocus>
                                                    @error('yearsOfExperience')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div
                                                class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                                            <div class="form-group row">
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <label>If experienced please give us an idea of your average
                                                            piece
                                                            count per hour for</label>
                                                    </div>
                                                    <div class="col-xl-3">
                                                        <div class="form-group">
                                                            <label>Soft lines </label>
                                                            <input type="hidden" class="form-control"
                                                                   name="pieceCountName[]" required
                                                                   value="Soft lines">
                                                            <input type="number" class="form-control"
                                                                   name="pieceCount[]">
                                                            @if($errors->has('pieceCountName'))
                                                                <div
                                                                    class="error">{{ $errors->first('pieceCountName') }}</div>
                                                            @endif
                                                            @if($errors->has('pieceCount'))
                                                                <div
                                                                    class="error">{{ $errors->first('pieceCount') }}</div>
                                                            @endif
                                                            <span
                                                                class="form-text text-muted">Please enter number.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3">
                                                        <div class="form-group">
                                                            <label>Hard lines </label>
                                                            <input type="hidden" class="form-control"
                                                                   name="pieceCountName[]" required
                                                                   value="Hard lines">
                                                            <input type="number" class="form-control"
                                                                   name="pieceCount[]">
                                                            <span
                                                                class="form-text text-muted">Please enter number.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3">
                                                        <div class="form-group">
                                                            <label>Grocery </label>
                                                            <input type="hidden" class="form-control"
                                                                   name="pieceCountName[]" required
                                                                   value="Grocery">
                                                            <input type="number" class="form-control"
                                                                   name="pieceCount[]">
                                                            <span
                                                                class="form-text text-muted">Please enter number.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3">
                                                        <div class="form-group">
                                                            <label>Dollar Stores </label>
                                                            <input type="hidden" class="form-control"
                                                                   name="pieceCountName[]" required
                                                                   value="Dollar Stores">
                                                            <input type="number" class="form-control"
                                                                   name="pieceCount[]">
                                                            <span
                                                                class="form-text text-muted">Please enter number.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div
                                            class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Do you have
                                                Experience counting pharmacy labs</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="form-group">
                                                    <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                        <input type="radio" name="countingPharmacy" required
                                                               value="Yes"> Yes
                                                        <span></span>
                                                    </label> &nbsp;&nbsp;&nbsp;
                                                    <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                        <input type="radio" name="countingPharmacy" required
                                                               value="No"> No
                                                        <span></span>
                                                    </label>
                                                    @error('countingPharmacy')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                    <span
                                                        class="form-text text-muted">Please select one.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Do you have
                                                Retail Experience </label>
                                            <div class="form-group">
                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                    <input type="radio" name="retailExperienceStatus" required
                                                           value="Yes">Yes
                                                    <span></span>
                                                </label> &nbsp;&nbsp;&nbsp;
                                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                                    <input type="radio" name="retailExperienceStatus" required
                                                           value="No"> No
                                                    <span></span>
                                                </label>
                                                @error('retailExperienceStatus')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <span class="form-text text-muted">Please select one.</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Years Of
                                                Experience </label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="form-group">
                                                    <input type="number" id="retailExperience"
                                                           class="form-control @error('retailExperience') is-invalid @enderror"
                                                           name="retailExperience"
                                                           value="{{ old('retailExperience') }}" required
                                                           autocomplete="retailExperience" autofocus>
                                                    @error('retailExperience')
                                                    <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6"></div>
                                    <div class="col-lg-6">
                                        <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
                                            role="tablist">
                                            <li class="nav-item">
                                                <a class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                                   data-toggle="tab" href="#kt_user_edit_tab_2" role="tab">
                                                    Previous
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                                   data-toggle="tab" href="#kt_user_edit_tab_4" role="tab">
                                                    Next
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div
                                class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                        </div>
                        <div class="tab-pane" id="kt_user_edit_tab_4" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-2"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm active btn btn-label-info-o2">
                                                        Availability :
                                                    </h3>
                                                    <p>Please select your available start and end time of each day of
                                                        the week</p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label class="col-xl-2"></label>
                                                <div class="col-lg-10 col-xl-6">
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" readonly
                                                                       value="Monday">
                                                                <input type="hidden" class="form-control"
                                                                       name="dayName[]"
                                                                       value="Monday">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control"
                                                                       name="fromTime[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control" name="toTime[]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" readonly
                                                                       value="Tuesday">
                                                                <input type="hidden" class="form-control"
                                                                       name="dayName[]"
                                                                       value="Tuesday">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control"
                                                                       name="fromTime[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control" name="toTime[]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" readonly
                                                                       value="Wednesday">
                                                                <input type="hidden" class="form-control"
                                                                       name="dayName[]"
                                                                       value="Wednesday">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control"
                                                                       name="fromTime[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control" name="toTime[]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" readonly
                                                                       value="Thursday">
                                                                <input type="hidden" class="form-control"
                                                                       name="dayName[]"
                                                                       value="Thursday">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control"
                                                                       name="fromTime[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control" name="toTime[]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" readonly
                                                                       value="Friday">
                                                                <input type="hidden" class="form-control"
                                                                       name="dayName[]"
                                                                       value="Friday">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control"
                                                                       name="fromTime[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control" name="toTime[]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" readonly
                                                                       value="Saturday">
                                                                <input type="hidden" class="form-control"
                                                                       name="dayName[]"
                                                                       value="Saturday">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control"
                                                                       name="fromTime[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control" name="toTime[]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" readonly
                                                                       value="Sunday">
                                                                <input type="hidden" class="form-control"
                                                                       name="dayName[]"
                                                                       value="Sunday">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control"
                                                                       name="fromTime[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="form-group">
                                                                <input type="time" class="form-control" name="toTime[]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-2">
                                                </div>
                                                <div class="col-xl-8">
                                                    <div class="form-group">
                                                        <label>Current Status </label>
                                                        <select class="form-control" name="currentStatus">
                                                            <option value="Available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                        </select>
                                                        @error('currentStatus')
                                                        <span class="invalid-feedback" role="alert">
                                                             <strong>{{ $message }}</strong>
                                                         </span>
                                                        @enderror
                                                        @if($errors->has('currentStatus'))
                                                            <div
                                                                class="error">{{ $errors->first('currentStatus') }}</div>
                                                        @endif
                                                        <span class="form-text text-muted">Please select your current status.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-6">
                                            <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
                                                role="tablist">
                                                <li class="nav-item">
                                                    <a class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                                       data-toggle="tab" href="#kt_user_edit_tab_3" role="tab">
                                                        Previous
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <input type="submit"
                                                           class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function getExperienceValue(value) {
            if (value === "Yes") {
                $(".trialEndsAt").css('display', 'block');
            } else {
                $(".trialEndsAt").css('display', 'none');
            }
        }

        function changeType() {
            var experienceCompany = document.getElementById('experienceCompanyName').value;
            if (experienceCompany === "Other") {
                var div = document.getElementsByClassName('typeField');
                for (var i = 0; i < div.length; i++) {
                    div[i].style.display = "block";
                }
                $(div).slice(1).css('margin-top', "12px");
            } else {
                var div = document.getElementsByClassName('typeField');
                for (var i = 0; i < div.length; i++) {
                    div[i].style.display = "none";
                }
            }
        }
    </script>
@endsection

