@extends('_layouts.admin')
@section('content')
    <div class="kt-login__body " style="margin-top: 20%">

        <!--begin::Signin-->
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3>@yield('form-title')</h3>
            </div>
            @include('_layouts.flash-message')
            <div class="kt-login__body">
                <!--begin::Signin-->
                <div class="kt-login__form">
                    <div class="kt-login__title">
                        <h3>Sign In</h3>
                    </div>
                    <form method="POST" action="{{ route('login') }}" class="kt-form" novalidate="novalidate"
                          id="kt_login_form">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   placeholder="Email"
                                   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="password" placeholder="Password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <!--begin::Action-->
                        {{--                <div class="kt-login__actions">--}}
                        {{--                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                        {{--                    <label class="form-check-label" for="remember">--}}
                        {{--                        {{ __('Remember Me') }}--}}
                        {{--                    </label>--}}
                        {{--                </div>--}}
                        <div class="kt-login__actions">
{{--                            @if (Route::has('password.request'))--}}
{{--                                <a href="{{ route('password.request') }}" class="kt-link kt-login__link-forgot">--}}
{{--                                    Forgot Password ?--}}
{{--                                </a>--}}
{{--                            @endif--}}
                            <button id="kt_login_signin_submit"
                                    class="btn btn-primary btn-elevate kt-login__btn-primary">Sign
                                In
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
