@extends('_layouts.index')
@section('content')
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
            <!-- begin:: Content Head -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-subheader__main">
                        <h3 class="kt-subheader__title">
                            Admin Profile
                        </h3>
                        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                        <div class="kt-subheader__group" id="kt_subheader_search">
                            <span class="kt-subheader__desc" id="kt_subheader_total">
                                {{  Auth::guard('admin')->user()->name }}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        @include('_layouts.flash-message')
        <!-- end:: Content Head -->
            <!-- begin:: Content -->
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet__body">
                        <form method="POST" action="{{ route('admin.update') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{  Auth::guard('admin')->user()->id }}" name="id">
                            @if($errors->has('id'))
                                <div
                                    class="error">{{ $errors->first('id') }}</div>
                            @endif
                            <div class="tab-content">
                                <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                                    <div class="kt-form kt-form--label-right">
                                        <div class="kt-form__body">
                                            <div class="kt-section kt-section--first">
                                                <div class="kt-section__body">
                                                    <div class="row">
                                                        <label class="col-xl-3"></label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class=" kt-section__content--solid">
                                                                <h3 class="kt-section__title kt-section__title-sm active btn btn-label-info-o2">
                                                                    Profile :
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Image</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="form-group row">
                                                                <div class="col-lg-9 col-xl-6">
                                                                    <div class="kt-avatar kt-avatar--outline"
                                                                         id="kt_user_avatar">
                                                                        @if( Auth::guard('admin')->user()->adminImage != "")
                                                                            <div class="kt-avatar__holder"
                                                                                 style="background-image: url('{{ URL::to('/') }}/{{  Auth::guard('admin')->user()->adminImage->path }}')">
                                                                            </div>
                                                                        @endif
                                                                        @if( Auth::guard('admin')->user()->adminImage == "")
                                                                            <div class="kt-avatar__holder"
                                                                                 style="background-image: url({{asset('assets/media/users/100_13.jpg')}})"></div>
                                                                        @endif
                                                                        <label class="kt-avatar__upload"
                                                                               data-toggle="kt-tooltip"
                                                                               title=""
                                                                               data-original-title="Change avatar">
                                                                            <i class="fa fa-pen"></i>
                                                                            <input type="file" name="image">
                                                                        </label>
                                                                        <span class="kt-avatar__cancel"
                                                                              data-toggle="kt-tooltip"
                                                                              title=""
                                                                              data-original-title="Cancel avatar">
                                                                            <i class="fa fa-times"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Name</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input type="text" name="name" id="name"
                                                                   class="form-control @error('name') is-invalid @enderror"
                                                                   placeholder="Name"
                                                                   value="{{  Auth::guard('admin')->user()->name }}"
                                                                   required autocomplete="name" autofocus>
                                                            @if($errors->has('name'))
                                                                <div
                                                                    class="error">{{ $errors->first('name') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input id="email" type="email"
                                                                   class="form-control @error('email') is-invalid @enderror"
                                                                   name="email" placeholder="Email"
                                                                   value="{{  Auth::guard('admin')->user()->email }}"
                                                                   required
                                                                   autocomplete="email">
                                                            @if($errors->has('email'))
                                                                <div class="error">{{ $errors->first('email') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input id="password" type="password"
                                                                   class="form-control @error('password') is-invalid @enderror"
                                                                   name="password" autocomplete="new-password"
                                                                   placeholder="Password">
                                                            @if($errors->has('password'))
                                                                <div
                                                                    class="error">{{ $errors->first('password') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Confirmed
                                                            Password</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input id="password-confirm" type="password"
                                                                   class="form-control" name="password_confirmation"
                                                                   placeholder="Confirmed Password"
                                                                   autocomplete="new-password">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="input-group">
                                                                <textarea id="address"
                                                                          class="form-control @error('address') is-invalid @enderror"
                                                                          name="address"
                                                                          placeholder="Address">{{  Auth::guard('admin')->user()->address }}</textarea>
                                                                @if($errors->has('address'))
                                                                    <div
                                                                        class="error">{{ $errors->first('address') }}</div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6"></div>
                                        <div class="col-lg-6">
                                            <input type="submit"
                                                   class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                                   value="Update">
                                            <input type="reset"
                                                   class="btn btn-default btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                                   value="Reset">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end:: Content -->
        </div>
    </div>
    <!-- end:: Content -->


@endsection



