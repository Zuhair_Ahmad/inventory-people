@foreach($user as $row)
    <tr>
        <td>{{ $row->firstName}} &nbsp; {{ $row->lastName}}</td>
        <td>{{ $row->email }}</td>
        <td>{{ $row->city }}</td>
        <td>{{ $row->country }}</td>
        <td>{{ $row->phone }}</td>
        <td>{{ $row->inventoryExperience }}</td>
        <td> {{ $row->currentStatus }}</td>
        <td>
            <a href="{{url('/admin/user/edit', $row->id)}}">
                <i class="fa fa-edit"></i>
            </a> &nbsp;
        </td>
    </tr>
@endforeach
<tr>
    <td colspan="8" align="center">
        {{ $user->links() }}
    </td>
</tr>
