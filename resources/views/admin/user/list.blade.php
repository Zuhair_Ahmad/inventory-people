@extends('_layouts.index')
@section('content')
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content"
         style="margin-top: -2%">
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-sm-12">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head" style="align-items: center">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    List of Users
                                </h3>
                            </div>
                        </div>
                        <div align="right" class="mt-2 mr-4">
                            <a href="{{ route('export_excel.excel') }}" class="btn btn-outline-secondary"> <i class="fa fa-file-export"></i> Export to CSV</a>
                        </div>
                        @include('_layouts.flash-message')
                        <div class="kt-portlet__body">
                            <!--begin::Section-->
                            <div class="kt-section">
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                        <form action="{{ route('user.filter') }}" method="POST"
                                              enctype="multipart/form-data"
                                              class="kt-form kt-form--label-right">
                                            {{csrf_field()}}
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Inventory experience</label>
                                                        <select class="form-control" name="experience">
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Country</label>
                                                        <select class="form-control" name="country">
                                                            <option value="All">All</option>
                                                            <option value="US">US</option>
                                                            <option value="Canada">Canada</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Retail Experience</label>
                                                        <select class="form-control" name="retailExperience">
                                                            <option value="All">All</option>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Current Status</label>
                                                        <select class="form-control" name="status">
                                                            <option value="All">All</option>
                                                            <option value="Available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 mt-4">
                                                        <input type="submit" value="Filter"
                                                               class="btn btn-group-lg btn-success">
                                                        <a href="{{ route('user.list') }}"
                                                           class="btn btn-group-lg btn-default">Reset</a>
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                </div>
                                                <div class="table-responsive mt-5">
                                                    <table class="table table-striped table-bordered ">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>City</th>
                                                            <th>Country</th>
                                                            <th>Phone</th>
                                                            <th>Inventory<br>Experience</th>
                                                            <th>Retail<br>Experience</th>
                                                            <th>Status</th>
                                                            <th>-</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($user as $row)
                                                            <tr>
                                                                <td>{{ $row->firstName}} &nbsp; {{ $row->lastName}}</td>
                                                                <td>{{ $row->email }}</td>
                                                                <td>{{ $row->city }}</td>
                                                                <td>{{ $row->country }}</td>
                                                                <td>{{ $row->phone }}</td>
                                                                <td>{{ $row->inventoryExperience }}</td>
                                                                <td>{{ $row->retailExperienceStatus }}</td>
                                                                <td> {{ $row->currentStatus }}</td>
                                                                <td>
                                                                    <a href="{{url('/admin/user/edit', $row->id)}}">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a> &nbsp;
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="9" align="center">
                                                                {{ $user->links() }}
                                                            </td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>

@endsection



