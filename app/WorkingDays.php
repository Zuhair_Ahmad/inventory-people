<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingDays extends Model
{
    protected $table = 'working_days';
    protected $fillable = [
        'name',
        'fromTime',
        'toTime'
    ];
}
