<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceAveragePieceCount extends Model
{
    protected $table = 'experience_average_piece_count';
    protected $fillable = [
        'user_id',
        'name',
        'countPerHour'
    ];
}
