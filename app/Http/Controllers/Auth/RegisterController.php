<?php

namespace App\Http\Controllers\Auth;

use App\ExperienceAveragePieceCount;
use App\Http\Controllers\Controller;
use App\Http\Traits\FileUpload;
use App\Image;
use App\Providers\RouteServiceProvider;
use App\User;
use App\WorkingDays;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use FileUpload;
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstName' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => 'required',
            'mobile' => 'required',
            'street' => 'required',
            'doorNumber' => 'required',
            'appartement' => 'required',
            'city' => 'required',
            'country' => 'required',
            'address' => 'required',
            'inventoryExperience' => 'required',
            'countingPharmacy' => 'required',
            'retailExperienceStatus' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $user = User::create([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'mobile' => $data['mobile'],
            'street' => $data['street'],
            'doorNumber' => $data['doorNumber'],
            'appartement' => $data['appartement'],
            'city' => $data['city'],
            'country' => $data['country'],
            'postalCode' => $data['postalCode'],
            'address' => $data['address'],
            'inventoryExperience' => $data['inventoryExperience'],
            'experienceCompanyName' => $data['experienceCompanyName'],
            'otherCompany' => $data['otherCompany'],
            'level' => $data['level'],
            'yearsOfExperience' => $data['yearsOfExperience'],
            'countingPharmacy' => $data['countingPharmacy'],
            'retailExperienceStatus' => $data['retailExperienceStatus'],
            'retailExperience' => $data['retailExperience'],
            'currentStatus' => $data['currentStatus'],
        ]);

        $userId = $user->id;

        if ($_FILES['image']['name'] != "") {
            $images = [];
            $image = $data['image'];
            $userImage = new Image();
            $this->uploadUserImg($image, $userImage, 'path', null, $userId);
            $images[] = $userImage;
            $user->userImage()->saveMany($images, $user);
        }


        $experienceArr = array();
        $daysArr = array();
        if (isset($userId)) {
            array_push($experienceArr, $data);
            array_push($daysArr, $data);
        } else {
            $experienceArr = $data;
            $daysArr = $data;
        }
        foreach ($experienceArr as $key => $value) {
            foreach ($value['pieceCountName'] as $k => $v) {
                $pieceCount = $value['pieceCount'][$k];
                $finalData = array('user_id' => $userId, 'name' => $v, 'countPerHour' => $pieceCount);
                ExperienceAveragePieceCount::insert($finalData);
            }
        }

        foreach ($daysArr as $key => $value) {
            foreach ($value['dayName'] as $k => $v) {
                $fromTime = $value['fromTime'][$k];
                $toTime = $value['toTime'][$k];
                $daysData = array('user_id' => $userId, 'name' => $v, 'fromTime' => $fromTime, 'toTime' => $toTime);
                WorkingDays::insert($daysData);
            }
        }
        return $user;

    }
}
