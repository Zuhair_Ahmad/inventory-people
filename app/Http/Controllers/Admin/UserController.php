<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Exports\UserExport;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $user = User::orderBy('id', 'asc')->paginate(10);
        return view('admin.user.list', compact('user'));
    }

    public function filter(Request $request)
    {
        $experience = $request->experience;
        $country = $request->country;
        $retailExperience = $request->retailExperience;
        $status = $request->status;
        $user =  User::getFilterUsers($experience,$country,$retailExperience,$status);
        return view('admin.user.list', compact('user'));
    }

    public function edit($id)
    {
        try {
            $user = User::find($id);
            return view('admin.user.edit', compact('user'));
        } catch (\Exception $e) {
            return back()->with('error', 'Oops, something was not right');
        }
    }

    public function export()
    {
        return \Maatwebsite\Excel\Facades\Excel::download(new UserExport(), 'user.csv');
    }
}
