<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Traits\FileUpload;
use App\Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    use FileUpload;

    public function index(Request $request)
    {
        try {
            return view('admin.home', compact('user'));
        } catch (\Exception $e) {
            return back()->with('error', 'Oops, something was not right in user page');
        }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'unique:admins,email,' . $id,
                'password' => 'nullable|between:6,24,password' . $id,
                'password_confirmation' => 'same:password',
                'address' => 'required',
            ]);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }
            $user = Admin::where('id', $id)->first();
            $password = $user->password;
            $user->fill($request->only([
                'name',
                'email',
                'address',
            ]));
            if (!empty($request->password)) {
                $user->password = Hash::make($request->password);
            } else {
                $user->password = $password;
            }
            $user->save();
            if ($request->hasFile('image')) {
                $images = [];
                $image = $request->file('image');
                $userImage = new Image();
                $this->uploadAdminImg($image, $userImage, 'path', null, $id);
                $images[] = $userImage;
                $user->adminImage()->saveMany($images, $user);
            }

            return back()->with('success', 'Profile Updated Successfully!');
        } catch (\Exception $e) {
            return response()->json([
                'response' => $e
            ], 400);
        }
    }


}
