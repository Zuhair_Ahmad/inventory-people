<?php

namespace App\Http\Controllers;

use App\ExperienceAveragePieceCount;
use App\Http\Traits\FileUpload;
use App\Image;
use App\User;
use App\WorkingDays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    use FileUpload;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }

    public function update(Request $request)
    {
        $id = $request->id;
        try {
            $validator = Validator::make($request->all(), [
                'firstName' => 'required',
                'lastName' => 'required',
                'email' => 'unique:users,email,' . $id,
                'password' => 'nullable|between:6,24,password' . $id,
                'password_confirmation' => 'same:password',
                'phone' => 'required',
                'mobile' => 'required',
                'street' =>'required',
                'doorNumber' => 'required',
                'appartement' => 'required',
                'city' =>'required',
                'country' => 'required',
                'postalCode' => 'required',
                'address' => 'required',
                'inventoryExperience' => 'required',
                'experienceCompanyName' =>'required',
                'level' => 'required',
                'countingPharmacy' => 'required',
                'retailExperienceStatus' => 'required',
                'retailExperience' => 'required',
                'currentStatus' =>'required',
            ]);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }
            $user = User::where('id', $id)->first();
            $password = $user->password;
            $user->fill($request->only([
                'firstName',
                'lastName',
                'email',
                'phone',
                'mobile',
                'street',
                'doorNumber',
                'appartement',
                'city',
                'country',
                'postalCode',
                'address',
                'inventoryExperience',
                'experienceCompanyName',
                'otherCompany',
                'level',
                'yearsOfExperience',
                'countingPharmacy',
                'retailExperienceStatus',
                'retailExperience',
                'currentStatus',
            ]));
            if (!empty($request->password)) {
                $user->password = Hash::make($request->password);
            } else {
                $user->password = $password;
            }
            $user->save();
            if ($request->hasFile('image')) {
                $images = [];
                $image = $request->file('image');
                $userImage = new Image();
                $this->uploadUserImg($image, $userImage, 'path', null, $id);
                $images[] = $userImage;
                $user->userImage()->saveMany($images, $user);
            }

            $json        = $request->all();
            $data        = $json;
            $experienceArr = array();
            if (isset($data["id"])) {
                array_push($experienceArr, $data);
            } else {
                $experienceArr = $data;
            }
            ExperienceAveragePieceCount::where('user_id', $id)->delete();
            foreach ($experienceArr as $key => $value) {
                foreach ($value['pieceCountName'] as $k => $v) {
                    $pieceCount = $value['pieceCount'][$k];
                    $finalData = array('user_id' => $id, 'name' => $v, 'countPerHour' => $pieceCount);
                    ExperienceAveragePieceCount::insert($finalData);
                }
            }

            $daysJson        = $request->all();
            $daysData        = $daysJson;
            $daysArr = array();
            if (isset($daysData["id"])) {
                array_push($daysArr, $daysData);
            } else {
                $daysArr = $daysData;
            }
            WorkingDays::where('user_id', $id)->delete();
            foreach ($daysArr as $key => $value) {
                foreach ($value['dayName'] as $k => $v) {
                    $fromTime = $value['fromTime'][$k];
                    $toTime = $value['toTime'][$k];
                    $daysData = array('user_id' => $id, 'name' => $v, 'fromTime' => $fromTime, 'toTime' => $toTime);
                    WorkingDays::insert($daysData);
                }
            }
            return back()->with('success', 'Profile Updated Successfully!');
        } catch (\Exception $e) {
            return response()->json([
                'response' => $e
            ], 400);
        }
    }
}
