<?php

namespace App\Http\Traits;

use App\Image;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait FileUpload
{
    private function updateModelProperty($filePath, $modelObject, $propertyToUpdate, $disk)
    {
        if ($filePath) {
            if (!is_null($modelObject->$propertyToUpdate) && $modelObject->$propertyToUpdate != '') {
                $this->removeExistingFile($filePath, $disk);
            }
            $modelObject->$propertyToUpdate = $filePath;
        }
    }

    private function removeExistingFile($filePath, $disk)
    {
        if (Storage::disk($disk)->has($filePath)) {
            Storage::disk($disk)->delete($filePath);
        }
    }

    public function uploadUserImg(UploadedFile $uploadedFile, $modelObject, $propertyToUpdate, $filename = null, $objectId = null, $disk = "public")
    {
        $imageDelete = Image::where('image_type', 'App\User')->where('image_id', $objectId)->get();
        if (count($imageDelete) > -1) {
            foreach ($imageDelete as $image) {
                $image_path = $image->path;
                if (file_exists($image_path)) {
                    Storage::disk($disk)->delete($image_path);
                }
                Image::destroy($image->id);
            }
            $name = !is_null($filename) ? $filename : substr($uploadedFile->getClientOriginalName(), 0, strrpos($uploadedFile->getClientOriginalName(), "."));
            $psudoContainer = ($objectId) ? '/' . $objectId : '';
            $folder = 'uploads/user' . strtolower(substr(get_class($modelObject), strrpos(get_class($modelObject), '\\') + 1)) . $psudoContainer;
            $file = $uploadedFile->storeAs($folder, sprintf('%s_%s', str_replace(' ', '_', $name), str_replace(' ', '_', microtime())) . '.' . $uploadedFile->getClientOriginalExtension(), [
                'disk' => $disk
            ]);
            $this->updateModelProperty($file, $modelObject, $propertyToUpdate, $disk);
            return $file;
        } else {
            $name = !is_null($filename) ? $filename : substr($uploadedFile->getClientOriginalName(), 0, strrpos($uploadedFile->getClientOriginalName(), "."));
            $psudoContainer = ($objectId) ? '/' . $objectId : '';
            $folder = 'uploads/user' . strtolower(substr(get_class($modelObject), strrpos(get_class($modelObject), '\\') + 1)) . $psudoContainer;
            $file = $uploadedFile->storeAs($folder, sprintf('%s_%s', str_replace(' ', '_', $name), str_replace(' ', '_', microtime())) . '.' . $uploadedFile->getClientOriginalExtension(), [
                'disk' => $disk
            ]);
            $this->updateModelProperty($file, $modelObject, $propertyToUpdate, $disk);
            return $file;
        }
    }

    public function deleteUserImg($id, $disk = "public")
    {
        $imageDelete = Image::where('image_type', 'App\User')->where('image_id', $id)->get();
        foreach ($imageDelete as $image) {
            $image_path = $image->path;
            if (file_exists($image_path)) {
                Storage::disk($disk)->delete($image_path);
            }
            Image::destroy($image->id);
        }
    }

    public function uploadAdminImg(UploadedFile $uploadedFile, $modelObject, $propertyToUpdate, $filename = null, $objectId = null, $disk = "public")
    {
        $imageDelete = Image::where('image_type', 'App\Admin')->where('image_id', $objectId)->get();
        if (count($imageDelete) > -1) {
            foreach ($imageDelete as $image) {
                $image_path = $image->path;
                if (file_exists($image_path)) {
                    Storage::disk($disk)->delete($image_path);
                }
                Image::destroy($image->id);
            }
            $name = !is_null($filename) ? $filename : substr($uploadedFile->getClientOriginalName(), 0, strrpos($uploadedFile->getClientOriginalName(), "."));
            $psudoContainer = ($objectId) ? '/' . $objectId : '';
            $folder = 'uploads/admin' . strtolower(substr(get_class($modelObject), strrpos(get_class($modelObject), '\\') + 1)) . $psudoContainer;
            $file = $uploadedFile->storeAs($folder, sprintf('%s_%s', str_replace(' ', '_', $name), str_replace(' ', '_', microtime())) . '.' . $uploadedFile->getClientOriginalExtension(), [
                'disk' => $disk
            ]);
            $this->updateModelProperty($file, $modelObject, $propertyToUpdate, $disk);
            return $file;
        } else {
            $name = !is_null($filename) ? $filename : substr($uploadedFile->getClientOriginalName(), 0, strrpos($uploadedFile->getClientOriginalName(), "."));
            $psudoContainer = ($objectId) ? '/' . $objectId : '';
            $folder = 'uploads/admin' . strtolower(substr(get_class($modelObject), strrpos(get_class($modelObject), '\\') + 1)) . $psudoContainer;
            $file = $uploadedFile->storeAs($folder, sprintf('%s_%s', str_replace(' ', '_', $name), str_replace(' ', '_', microtime())) . '.' . $uploadedFile->getClientOriginalExtension(), [
                'disk' => $disk
            ]);
            $this->updateModelProperty($file, $modelObject, $propertyToUpdate, $disk);
            return $file;
        }
    }

    public function deleteAdminImg($id, $disk = "public")
    {
        $imageDelete = Image::where('image_type', 'App\Admin')->where('image_id', $id)->get();
        foreach ($imageDelete as $image) {
            $image_path = $image->path;
            if (file_exists($image_path)) {
                Storage::disk($disk)->delete($image_path);
            }
            Image::destroy($image->id);
        }
    }

}
