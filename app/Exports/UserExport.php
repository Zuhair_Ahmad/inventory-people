<?php

namespace App\Exports;

use App\User;
use Illuminate\Support\Facades\Date;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromQuery,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::with(['experiencePieceCount', 'WorkingDays'])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'firstName',
            'lastName',
            'email',
            'email_verified_at',
            'street',
            'doorNumber',
            'appartement',
            'city',
            'country',
            'postalCode',
            'address',
            'phone',
            'mobile',
            'inventoryExperience',
            'experienceCompanyName',
            'otherCompany',
            'level',
            'yearsOfExperience',
            'countingPharmacy',
            'retailExperienceStatus',
            'retailExperience',
            'currentStatus',
            'createdAt',
            'updatedAt',
        ];
    }
    public function query()
    {
        return User::query();
        /*you can use condition in query to get required result
         return Bulk::query()->whereRaw('id > 5');*/
    }
    public function map($bulk): array
    {
        return [
            $bulk->id,
            $bulk->firstName,
            $bulk->lastName,
            $bulk->email,
            $bulk->email_verified_at,
            $bulk->street,
            $bulk->doorNumber,
            $bulk->appartement,
            $bulk->city,
            $bulk->country,
            $bulk->postalCode,
            $bulk->address,
            $bulk->phone,
            $bulk->mobile,
            $bulk->inventoryExperience,
            $bulk->experienceCompanyName,
            $bulk->otherCompany,
            $bulk->level,
            $bulk->yearsOfExperience,
            $bulk->countingPharmacy,
            $bulk->retailExperienceStatus,
            $bulk->retailExperience,
            $bulk->currentStatus,
            Date::dateTimeToExcel($bulk->created_at),
            Date::dateTimeToExcel($bulk->updated_at),
        ];
    }
}
