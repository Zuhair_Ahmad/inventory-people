<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
//class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'email',
        'password',
        'street',
        'doorNumber',
        'appartement',
        'city',
        'country',
        'postalCode',
        'address',
        'phone',
        'mobile',
        'inventoryExperience',
        'experienceCompanyName',
        'otherCompany',
        'level',
        'yearsOfExperience',
        'countingPharmacy',
        'retailExperienceStatus',
        'retailExperience',
        'currentStatus',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userImage()
    {
        return $this->morphOne(Image::class, 'image');
    }

    public function experiencePieceCount()
    {
        return $this->hasMany(ExperienceAveragePieceCount::class, 'user_id', 'id');
    }

    public function WorkingDays()
    {
        return $this->hasMany(WorkingDays::class, 'user_id', 'id');
    }

    public static function getUserList($searchTerm, $sort_by, $sort_type)
    {
        return self::select([
                'users.*',
            ]
        )->where(function ($query) use ($searchTerm, $sort_by, $sort_type) {
            if ($searchTerm) {
                $query->where('users.firstName', 'like', '%' . $searchTerm . '%')
                    ->orWhere('users.lastName', 'like', '%' . $searchTerm . '%')
                    ->orWhere('users.email', 'like', '%' . $searchTerm . '%')
                    ->orWhere('users.city', 'like', '%' . $searchTerm . '%')
                    ->orWhere('users.country', 'like', '%' . $searchTerm . '%')
                    ->orWhere('users.phone', 'like', '%' . $searchTerm . '%')
                    ->orWhere('users.inventoryExperience', 'like', '%' . $searchTerm . '%')
                    ->orWhere('users.currentStatus', 'like', '%' . $searchTerm . '%');
            }
        })->orderBy($sort_by, $sort_type)->paginate(50);
    }

    public static function getFilterUsers($experience, $country, $retailExperience, $status)
    {
        return self::select([
                'users.*',
            ]
        )->where(function ($query) use ($experience, $country, $retailExperience, $status) {
            if (($experience === "Yes" && $country === "All" && $retailExperience === "All" && $status === "All") || ($experience === "No" && $country === "All" && $retailExperience === "All" && $status === "All")) {
                $query->where('inventoryExperience', $experience);
                // w.r.t country
            } elseif (($experience === "Yes" && $country === "US" && $retailExperience === "All" && $status === "All") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "All" && $status === "All") || ($experience === "No" && $country === "US" && $retailExperience === "All" && $status === "All") || ($experience === "No" && $country === "Canada" && $retailExperience === "All" && $status === "All")) {
                $query->where('inventoryExperience', $experience)->where('country', $country);
                // Yes w.r.t Retail Experience
            } elseif (($experience === "Yes" && $country === "All" && $retailExperience === "Yes" && $status === "All") || ($experience === "No" && $country === "All" && $retailExperience === "Yes" && $status === "All")) {
                $query->where('inventoryExperience', $experience)->where('retailExperienceStatus', $retailExperience);
            } elseif (($experience === "Yes" && $country === "US" && $retailExperience === "Yes" && $status === "All") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "Yes" && $status === "All") || ($experience === "No" && $country === "US" && $retailExperience === "Yes" && $status === "All") || ($experience === "No" && $country === "Canada" && $retailExperience === "Yes" && $status === "All")) {
                $query->where('inventoryExperience', $experience)->where('country', $country)->where('retailExperienceStatus', $retailExperience);
                // No w.r.t Retail Experience
            } elseif (($experience === "Yes" && $country === "All" && $retailExperience === "No" && $status === "All") || ($experience === "No" && $country === "All" && $retailExperience === "No" && $status === "All")) {
                $query->where('inventoryExperience', $experience)->where('retailExperienceStatus', $retailExperience);
            } elseif (($experience === "Yes" && $country === "US" && $retailExperience === "No" && $status === "All") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "No" && $status === "All") || ($experience === "No" && $country === "US" && $retailExperience === "No" && $status === "All") || ($experience === "No" && $country === "Canada" && $retailExperience === "No" && $status === "All")) {
                $query->where('inventoryExperience', $experience)->where('country', $country)->where('retailExperienceStatus', $retailExperience);
                // Yes w.r.t Status
            } elseif (($experience === "Yes" && $country === "All" && $retailExperience === "All" && $status === "Available") || ($experience === "Yes" && $country === "All" && $retailExperience === "All" && $status === "Not Available")) {
                $query->where('inventoryExperience', $experience)->where('currentStatus', $status);
            } elseif (($experience === "Yes" && $country === "US" && $retailExperience === "All" && $status === "Available") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "All" && $status === "Available") || ($experience === "Yes" && $country === "US" && $retailExperience === "All" && $status === "Not Available") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "All" && $status === "Not Available")) {
                $query->where('inventoryExperience', $experience)->where('country', $country)->where('currentStatus', $status);
            } elseif (($experience === "Yes" && $country === "US" && $retailExperience === "Yes" && $status === "Available") || ($experience === "Yes" && $country === "US" && $retailExperience === "Yes" && $status === "Not Available") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "Yes" && $status === "Available") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "Yes" && $status === "Not Available") || ($experience === "Yes" && $country === "US" && $retailExperience === "No" && $status === "Available") || ($experience === "Yes" && $country === "US" && $retailExperience === "No" && $status === "Not Available") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "No" && $status === "Available") || ($experience === "Yes" && $country === "Canada" && $retailExperience === "No" && $status === "Not Available")) {
                $query->where('inventoryExperience', $experience)->where('country', $country)->where('retailExperienceStatus', $retailExperience)->where('currentStatus', $status);
            }
        })->paginate(10);
    }

}
