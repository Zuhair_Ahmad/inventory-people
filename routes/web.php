<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/home', ['as' => 'update', 'uses' => 'HomeController@update']);

Route::prefix('admin')->group(function() {
    Route::get('/login','Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\Auth\LoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'Admin\Auth\LoginController@logout')->name('admin.logout');
    Route::get('/home', 'Admin\HomeController@index')->name('admin.dashboard');
    Route::post('/home',  'Admin\HomeController@update')->name('admin.update');

    Route::get('/user/list', 'Admin\UserController@index')->name('user.list');
    Route::get('/user/edit/{id}', 'Admin\UserController@edit')->name('user.edit');
    Route::post('/user/filter', 'Admin\UserController@filter')->name('user.filter');



    Route::get('/export_excel/excel', 'Admin\UserController@export')->name('export_excel.excel');

}) ;

