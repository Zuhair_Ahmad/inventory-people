<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('street')->nullable();
            $table->string('doorNumber')->nullable();
            $table->string('appartement')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('postalCode')->nullable();
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('inventoryExperience')->nullable();
            $table->string('experienceCompanyName')->nullable();
            $table->string('otherCompany')->nullable();
            $table->string('level')->nullable();
            $table->string('yearsOfExperience')->nullable();
            $table->string('countingPharmacy')->nullable();
            $table->string('retailExperienceStatus')->nullable();
            $table->string('retailExperience')->nullable();
            $table->string('currentStatus')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
