<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Admin::truncate();
        \App\Admin::create([
            'name' => 'Inventory People',
            'email' => 'admin@inventorypeople.com',
            'password' => \Illuminate\Support\Facades\Hash::make('inventory@people')
        ]);
    }
}
